
; Drupal version
core = 7.x
api = 2

; Contrib modules
projects[debut_in_the_news][subdir] = contrib
projects[debut_in_the_news][version] = 1.0-beta1
projects[debut][subdir] = contrib
projects[debut_media][subdir] = contrib
projects[debut_link][subdir] = contrib
projects[ctools][subdir] = contrib
projects[features][subdir] = contrib
projects[strongarm][subdir] = contrib
projects[views][subdir] = contrib
projects[date][subdir] = contrib
